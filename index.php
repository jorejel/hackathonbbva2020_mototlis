<!doctype html>
<html class="no-js" lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Hackathon BBVA 2020 - Reto Huella Verde</title>
    <meta name="description" content="Hackathon BBVA 2020 - Reto Huella Verde">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- CSS here -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/slicknav.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/progressbar_barfiller.css">
    <link rel="stylesheet" href="assets/css/gijgo.css">
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <link rel="stylesheet" href="assets/css/animated-headline.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <!-- ? Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="assets/img/logo/loder.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
    <header>
        <!-- Header Start -->
            <?php include('include/menu.php');?>
        <!-- Header End -->
    </header>
    <main>
        
        <!-- slider Area Start-->
        <div class="slider-area position-relative">
            <div class="slider-active dot-style">
                <!-- Single Slider -->
                <div class="single-slider hero-overly slider-height slider-bg1 d-flex align-items-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-10">
                                <div class="hero__caption">
                                    <h1 data-animation="fadeInUp" data-delay=".2s">Huella Verde</h1>
                                    <P data-animation="fadeInUp" data-delay=".4s">Cambiemos nuestro mundo<br>para salvar el de todos<br><br><br></P>
                                    <!-- Hero-btn -->
                                    <div class="hero__btn">
                                        <a href="calculadoracasa.php" class="hero-btn mb-10"  data-animation="fadeInUp" data-delay=".8s">¿Cuánto contaminas en casa?</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Single Slider -->
                <div class="single-slider hero-overly slider-height slider-bg2 d-flex align-items-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-10">
                                <div class="hero__caption">
                                    <h1 data-animation="fadeInUp" data-delay=".2s">BBVA Open Innovation</h1>
                                    <P data-animation="fadeInUp" data-delay=".4s">Reto<br>Huella Verde</P>
                                    <!-- Hero-btn -->
                                    <div class="hero__btn">
                                        <a href="calculadora_auto_CO2.php" class="hero-btn mb-10"  data-animation="fadeInUp" data-delay=".8s">¿Cuánto contaminas en tu auto?</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Single Slider -->
                <div class="single-slider hero-overly slider-height slider-bg1 d-flex align-items-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-10">
                                <div class="hero__caption">
                                    <h1 data-animation="fadeInUp" data-delay=".2s">Crea el Wiki Verde</h1>
                                    <P data-animation="fadeInUp" data-delay=".4s">Registra el código de barras de todos los artículos eléctricos<br>y únete a la comunidad que está creando el inventario verde mundial</P>
                                    <!-- Hero-btn -->
                                    <div class="hero__btn">
                                        <a href="wikiverde.php" class="hero-btn mb-10"  data-animation="fadeInUp" data-delay=".8s">Sé parte del proyecto</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--? video_start -->
            <div class="video-area d-flex align-items-center justify-content-center">
                <div class="video-wrap position-relative">
                    <div class="video-icon" >
                        <a class="popup-video btn-icon" href="https://www.youtube.com/watch?v=BHACKCNDMW8"><i class="fas fa-play"></i></a>
                    </div>
                </div>
            </div>
            <!-- video_end -->

        </div>
        <!-- slider Area End-->
        <!--? Visit Our Tailor Start -->
        <div class="visit-tailor-area fix">
            <!--Right Contents  -->
            <div class="tailor-offers"></div>
            <!-- left Contents -->
            <div class="tailor-details">
                <span>Nuestra misión</span>
                <h2>La razón de ser de<br> Huella Xoxoktli</h2>
                <p>En el equipo Mototlis, buscamos desarrollar una innovadora plataforma web que permita a cada uno de nuestros usuarios calcular su huella de carbono de una manera eficiente. Todo esto podrá ser posible gracias a nuestro cluster wiki que contendrá información amplia sobre todos los productos que se utilizan alrededor del mundo y su impacto ambiental, misma información será recopilada gracias al escaneo del código de barras de los productos que se relaciona a nuestra base de datos única que contiene los datos sobre su impacto ecológico. 
</p>
                <p class="pera-bottom">Asimismo, gracias a nuestro cluster podremos calcular la huella de carbono de cada persona, empresas u hogar y darles recomendaciones sobre productos que ayuden a reducir su huella, créditos que ofrece BBVA accesibles según su perfil, el cálculo de ahorro económico a largo plazo y así concientizar a las personas para tener un mejor control de su impacto ambiental y económico.</p>
                <div class="footer-tittles">
                    <p>Atte.</p>
                    <h2>Equipo Mototlis</h2>
                </div>
            </div>
        </div>
        <!-- Visit Our Tailor End -->
        <!--? Services Area Start -->
        <div class="service-area section-padding30">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-11">
                        <div class="single-cat text-center mb-30">
                            <div class="cat-icon">
                                <img src="assets/img/gallery/amigable.jpg" alt="">
                            </div>
                            <div class="cat-cap">
                                <h5><a href="#">Amigable con el ambiente</a></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-11">
                        <div class="single-cat active text-center mb-30">
                            <div class="cat-icon">
                                <img src="assets/img/gallery/reducir.jpeg" alt="">
                            </div>
                            <div class="cat-cap">
                                <h5><a href="#">Ayuda a reducir el CO2</a></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-11">
                        <div class="single-cat text-center mb-30">
                            <div class="cat-icon">
                                <img src="assets/img/gallery/cuidar.jpg" alt="">
                            </div>
                            <div class="cat-cap">
                                <h5><a href="#">Cuida el planeta</a></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Services Area End -->
        <!--? About Area Start -->
        <section class="support-company-area fix pt-10">
            <div class="support-wrapper align-items-center">
                <div class="left-content">
                    <!-- section tittle -->
                    <div class="section-tittle section-tittle2 mb-30">
                        <span> Únete a nuestra comunidad</span>
                        <!--<h2>A lot of animals need protection</h2>-->
                    </div>
                    <div class="support-caption">
                        <p class="pera-top">En huella xoxoktli estamos creando una comunidad 
                        para crear el inventario verde mundial con información de los productos más ecológicos. 
                        Para poder darte las mejores recomendaciones de estos productos es importante 
                        tu colaboración con sólo escanear el código de barras de tus artículos electrónicos </p>
                        <a href="registro_productos.php" class="border-btn">Aportar a wiki verde</a>
                    </div>
                </div>
                <div class="right-content">
                    <!-- img -->
                    <div class="right-img">
                        <img src="assets/img/gallery/safe_in.png" alt="">
                    </div>
                    <div class="support-img-cap text-center d-flex">
                        <div class="single-one">
                            <span>300</span>
                            <p>Registros de aparatos electronicos</p>
                        </div>
                        <div class="single-two">
                            <span>50</span>
                            <p>Usuarios Registrados</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Area End -->
        <!--? Our Cases Start -->
        <div class="our-cases-area section-padding30">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-10 col-lg-10 ">
                        <!-- Section Tittle -->
                        <div class="section-tittle text-center mb-80">
                            <h2>Direferentes Calculadores</h2>
                            <p class="pl-20 pr-20">Aqui encontraras calculadoras para saber el CO2 que generas</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cases mb-40">
                            <div class="cases-img">
                                <img src="assets/img/gallery/casa.jpg" alt="" height=200px>
                            </div>
                            <div class="cases-caption">
                                <h3><a href="#">Calculadora CO2 de casa</a></h3>
                                <p>Calcula tu consumo de energía mensual en casa según el tipo de gas que utilices.</p>
                                <!-- Progress Bar -->
                                <div class="single-skill mb-15">
                                    <div class="bar-progress">
                                        <div id="bar1" class="barfiller">
                                            <div class="tipWrap">
                                                <span class="tip"></span>
                                            </div>
                                            <span class="fill" data-percentage="40" ></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- / progress -->
                                <div class="prices">
                                    <p><span>Huella de carbono 100,2000 </span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cases mb-40">
                            <div class="cases-img">
                                <img src="assets/img/gallery/coche.JPG" alt="" height=200px>
                            </div>
                            <div class="cases-caption">
                                <h3><a href="#">Calculadora CO2 de Automovil</a></h3>
                                <p>Conoce la huella de carbono de tu auto según tu tipo de gasolina y uso que le das.</p>
                                <!-- Progress Bar -->
                                <div class="single-skill mb-15">
                                    <div class="bar-progress">
                                        <div id="bar2" class="barfiller">
                                            <div class="tipWrap">
                                                <span class="tip"></span>
                                            </div>
                                            <span class="fill" data-percentage="70"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="prices">
                                    <p><span>Huella de carbono 200,000</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cases">
                            <div class="cases-img">
                                <img src="assets/img/gallery/oficina.jpeg" alt="" height=230px>
                            </div>
                            <div class="cases-caption">
                                <h3><a href="#">Calculadora CO2 de oficina</a></h3>
                                <p>Calculadora en construcción</p>
                                <!-- Progress Bar -->
                                <div class="single-skill mb-15">
                                    <div class="bar-progress">
                                        <div id="bar3" class="barfiller">
                                            <div class="tipWrap">
                                                <span class="tip"></span>
                                            </div>
                                            <span class="fill" data-percentage=""></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="prices">
                                    <p><span>En construcción</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Our Cases End -->
        <!--? Blog Area Start -->
        <!---<section class="home-blog-area pb-padding">
            <div class="container">
                <!-- Section Tittle 
                <div class="row justify-content-center">
                    <div class="col-xl-8 col-lg-9 col-md-11">
                        <div class="section-tittle text-center mb-90">
                            <h2>Últimas noticias</h2>
                            <p>Interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus nibh et justo  cursus id rutrum lorem imperdiet. Nunc ut sem vitae risus tristique posuere.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="home-blog-single mb-30">
                            <div class="blog-img-cap">
                                <div class="blog-img">
                                    <img src="assets/img/gallery/home-blog1.png" alt="">
                                </div>
                                <div class="blog-cap">
                                    <h3><a href="blog_details.html">Leverage agile frameworks to provide a robust synopsis</a></h3>
                                    <p>The automated process starts as soon as  your clothes go into the machine. Duis cursus, mi quis viverra ornare.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="home-blog-single mb-30">
                            <div class="blog-img-cap">
                                <div class="blog-img">
                                    <img src="assets/img/gallery/home-blog2.png" alt="">
                                </div>
                                <div class="blog-cap">
                                    <h3><a href="blog_details.html">Leverage agile frameworks to provide a robust synopsis</a></h3>
                                    <p>The automated process starts as soon as  your clothes go into the machine. Duis cursus, mi quis viverra ornare.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>--->
        <!-- Blog Area End -->

    </main>
            <?php include('include/footer.php');?>

      <!-- Scroll Up -->
      <div id="back-top" >
        <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
    </div>

    <!-- JS here -->

    <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="./assets/js/popper.min.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="./assets/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="./assets/js/owl.carousel.min.js"></script>
    <script src="./assets/js/slick.min.js"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src="./assets/js/wow.min.js"></script>
    <script src="./assets/js/animated.headline.js"></script>
    <script src="./assets/js/jquery.magnific-popup.js"></script>

    <!-- Date Picker -->
    <script src="./assets/js/gijgo.min.js"></script>
    <!-- Nice-select, sticky -->
    <script src="./assets/js/jquery.nice-select.min.js"></script>
    <script src="./assets/js/jquery.sticky.js"></script>
    <!-- Progress -->
    <script src="./assets/js/jquery.barfiller.js"></script>
    
    <!-- counter , waypoint,Hover Direction -->
    <script src="./assets/js/jquery.counterup.min.js"></script>
    <script src="./assets/js/waypoints.min.js"></script>
    <script src="./assets/js/jquery.countdown.min.js"></script>
    <script src="./assets/js/hover-direction-snake.min.js"></script>

    <!-- contact js -->
    <script src="./assets/js/contact.js"></script>
    <script src="./assets/js/jquery.form.js"></script>
    <script src="./assets/js/jquery.validate.min.js"></script>
    <script src="./assets/js/mail-script.js"></script>
    <script src="./assets/js/jquery.ajaxchimp.min.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/main.js"></script>
    
</body>
</html>