<?php

 require 'vendor/autoload.php';
 use Aws\Rekognition\RekognitionClient;
 
 $options = [
 'region' => 'us-west-2',
 'version' => 'latest',
 'debug'   => true
 ]; 


 $rekognition = new RekognitionClient($options);

 // Get local image
 $photo = 'horacio.jpg';
 $fp_image = fopen($photo, 'r');
 $image = fread($fp_image, filesize($photo));
 fclose($fp_image);
 // Call DetectFaces
 $result = $rekognition->DetectFaces(array(
 'Image' => array(
 'Bytes' => $image,
 ),
 'Attributes' => array('ALL')
 )
 );
 // Display info for each detected person
 print 'People: Image position and estimated age' . PHP_EOL;
 for ($n=0;$n<sizeof($result['FaceDetails']); $n++){
 print 'Position: ' . $result['FaceDetails'][$n]['BoundingBox']['Left'] . " "
 . $result['FaceDetails'][$n]['BoundingBox']['Top']
 . PHP_EOL
 . 'Age (low): '.$result['FaceDetails'][$n]['AgeRange']['Low']
 . PHP_EOL
 . 'Age (high): ' . $result['FaceDetails'][$n]['AgeRange']['High']
 . PHP_EOL . PHP_EOL;
 } 
?>