<?php
require_once('include/conn.php');

$sesioniniciada=false;
if(isset($_POST['login'])){
  $correo=$_POST['correo'];
  $psw=$_POST['psw'];
  $query1="SELECT ID,email,password,name  FROM usuarios WHERE email='$correo'";
  $result=mysqli_query($conn,$query1);
  $message="";
  if(mysqli_num_rows($result)!=0){
    while($data=mysqli_fetch_assoc($result)){
     $usuario_ID=$data['ID'];
     $name=$data['name'];
     $password=$data['password'];
     //$veri= password_verify($psw,$contrasena);
    }
    if(password_verify($psw,$password)){
      //echo 'Contraseña correcta!\n'." ";
      $sesioniniciada=true;
      session_start();
      $_SESSION['usuarios_ID']=$usuario_ID;
      $_SESSION['userName']=$name;
      $_SESSION['sesioniniciada']=true;
      header("Location: wikiverde.php");
    }else{
            //echo 'ERRRRRRRRRRRRRRRRRROR';

    }
  }
}
//  $contrasena = password_hash(123, PASSWORD_DEFAULT);

?>
<!DOCTYPE html>
<html lang="es_MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="imagenes/huella_xoxoktli.png/">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <style>
      html,
      body {
        height: 100%;
      }
      body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }
      .form-signin {
        width: 100%;
        max-width: 330px;
        padding: 15px;
        margin: auto;
      }
      .form-signin .checkbox {
        font-weight: 400;
      }
      .form-signin .form-control {
        position: relative;
        box-sizing: border-box;
        height: auto;
        padding: 10px;
        font-size: 16px;
      }
      .form-signin .form-control:focus {
        z-index: 2;
      }
      .form-signin input[type="email"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
      }
      .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
      }
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
  </head>
  <body class="text-center">

    <form method="POST" action="" class="form-signin">
     <img src="huella_xoxoktli.png" class="img-fluid" alt="huella_xoxoktli">
      <h1 class="h3 mb-3 font-weight-normal">Por favor inicia sesión</h1>
      <h4 class="h3 mb-3 font-weight-normal" id="estatus"></h4>
      <label for="inputEmail" class="sr-only">Usuario</label>
      <input type="text" id="inputEmail" name="correo" class="form-control" placeholder="Usuario" required autofocus>
      <label for="inputPassword" class="sr-only">Contraseña</label>
      <input type="password" id="inputPassword" name="psw" class="form-control" placeholder="Contraseña" required>
      <input type="hidden" name="accion_ini" value="1">
      <button class="btn btn-lg btn-primary btn-block" name="login" type="submit">Iniciar sesión</button>
      <p class="mt-5 mb-3 text-muted">Copyright &copy;<script>document.write(new Date().getFullYear());</script> Huella Xoxoktli <i class="fa fa-heart" aria-hidden="true"></i></p>
    </form>
  </body>
</html>