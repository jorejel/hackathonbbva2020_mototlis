<?php
session_start();
    $usuario_ID=$_SESSION['usuarios_ID'];
    $nombre=$_SESSION['userName'];
 
?>
<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Wiki Verde - Huella Xoxoktli</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

	<!-- CSS here -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="assets/css/slicknav.css">
	<link rel="stylesheet" href="assets/css/animate.min.css">
	<link rel="stylesheet" href="assets/css/hamburgers.min.css">
	<link rel="stylesheet" href="assets/css/magnific-popup.css">
	<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="assets/css/themify-icons.css">
	<link rel="stylesheet" href="assets/css/slick.css">
	<link rel="stylesheet" href="assets/css/nice-select.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<!--? Preloader Start -->
	<div id="preloader-active">
		<div class="preloader d-flex align-items-center justify-content-center">
			<div class="preloader-inner position-relative">
				<div class="preloader-circle"></div>
				<div class="preloader-img pere-text">
					<img src="assets/img/logo/loder.png" alt="">
				</div>
			</div>
		</div>
	</div>
	<!-- Preloader Start -->
	<header>
		<!-- Header Start -->
            <?php include('include/menu.php');?>

		<!-- Header End -->
	</header>
	<main>
		<!--? Hero Start -->
		<div class="slider-area2">
			<div class="slider-height2 d-flex align-items-center">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="hero-cap hero-cap2 pt-70">
								<h2>Wiki Verde</h2>
								<nav aria-label="breadcrumb">
									<ol class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.php">Home</a></li>
										<li class="breadcrumb-item"><a href="#">Wiki Verde</a></li> 
									</ol>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Hero End -->
		<!--? Start Sample Area -->
		<section class="sample-text-area">
			<div class="container box_1170">
			    <?php if($nombre!=NULL){ echo '<h3>Hola '.$nombre; } ?>
				<h3 class="text-heading">Qué es Wiki Verde</h3>
				<p class="sample-text">
					Wiki Verde será la comunidad más grande del mundo de productos y sus características ecológicas.
					Aquí todo el mundo puede subir productos que hay adquirido y registrar sus códigos de barras para crear la base de datos más grande
					del mundo de descripciones ecológicas. Asimismo muchos fabricantes están por sumarse a la iniciativa para que toda su linea de
					productos la puedas consultar aquí y conocer el impacto ambiental de cada dispositivo eléctronico.<br>
					<br><br><b>¿Deseas subir un producto?</b><br><br>
					Escanea su código de barras, captura el modelo, asocialo a una categoría y busca el Wattaje en la ficha técnica para que puedas
					subirlo a la base de datos verde y colaborativa más grande del mundo.<br><br>
					<h1>Bienvenido al proyecto</h1> 
				</p>
			</div>
		</section>
		<!-- End Sample Area -->
		
								<!--? Start Align Area -->
								<div class="whole-wrap">
									<div class="container box_1170">
										<div class="section-top-border">
											<h3 class="mb-30">El problema</h3>
											<div class="row">
												<div class="col-md-3">
													<img src="assets/img/gallery/casaVerde.jpg" alt="" class="img-fluid">
												</div>
												<div class="col-md-8 mt-sm-20">
													<p>Las emisiones de dióxido de carbono (CO<sub>2</sub>) tienen dos orígenes, naturales y antropogénicas, teniendo estas últimas un fuerte crecimiento en las últimas décadas (ver IPCC). <br>La concentración actual de CO<sub>2</sub> en el aire oscila alrededor de 416 ppm (2020), o 0,0415%, con algunas variaciones día-noche, estacionales (por la parte antrópica) y con picos de contaminación localizados. El contenido de CO<sub>2</sub> nunca ha sido tan elevado desde hace 2,1 millones de años.

La concentración de CO<sub>2</sub> en la atmósfera está aumentando desde finales del siglo XIX y el ritmo de aumento se aceleró a finales del siglo XX, pasando de 0,5 ppm/año en 1960 a 2 ppm/año en año 2000 (valor mínimo de 0,43 en 1992 y máximo de 3 ppm en 1998). Desde 2000, la tasa anual de aumento apenas ha cambiado.

Las emisiones antropogénicas mundiales están aumentando cada año: en 2007 las emisiones de CO<sub>2</sub> eran 2,0 veces mayores que en 1971. En 1990 fueron emitidas 20.878 Gt/año de CO<sub>2</sub> y en 2005 (26.402), o sea un aumento del 1,7% por año durante este período. La combustión de un litro de gasolina genera 2.3 kg de CO<sub>2</sub> y la de un litro de gasóleo 2,6 kg de CO<sub>2</sub>.
													</p>
												</div>
											</div>
										</div>
										
										<!--<div class="section-top-border text-right">
											<h3 class="mb-30">Right Aligned</h3>
											<div class="row">
												<div class="col-md-9">
													<p class="text-right">Over time, even the most sophisticated, memory packed computer can begin
														to run slow if we
														don’t do something to prevent it. The reason why has less to do with how computers are made
														and how they age and
														more to do with the way we use them. You see, all of the daily tasks that we do on our PC
														from running programs
														to downloading and deleting software can make our computer sluggish. To keep this from
														happening, you need to
														understand the reasons why your PC is getting slower and do something to keep your PC
														running at its best. You
													can do this through regular maintenance and PC performance optimization programs</p>
													<p class="text-right">Before we discuss all of the things that could be affecting your PC’s
														performance, let’s
													talk a little about what symptoms</p>
												</div>
												<div class="col-md-3">
													<img src="assets/img/elements/d.jpg" alt="" class="img-fluid">
												</div>
											</div>
										</div>-->
										
										<div class="section-top-border">
											<h3 class="center">Tres formas de sumarse</h3>
											<div class="row">
												<div class="col-md-4">
													<div class="single-defination">
														<h4 class="mb-20"><center>01</center><br>Escanea el código de barras de tus equipos</h4>
														<p>Escanea el código de barras con nuestro software y captura la información técnica ncesaria para determinar
														la huella de CO<sub>2</sub> del producto electrónico. Puede ser tu computadora, tu refri, la lavadora, en fin,
														todo lo que se conecte a la luz.</p>
													</div>
												</div>
												<div class="col-md-4">
													<div class="single-defination">
														<h4 class="mb-20"><center>02</center><br>Sube los artículos que tengas en casa</h4>
														<p>Aporta a la causa, sube todos los electrodomésticos que tengas en casa, tu computadora, TV, etc. Revisa cual es el Wattaje de cada uno
														y subelo a la base de datos. Con ésta información podemos calcular la huella de CO<sub>2</sub>.</p>
													</div>
												</div>
												<div class="col-md-4">
													<div class="single-defination">
														<h4 class="mb-20"><center>03</center><br>Crea tu inventario verde personalizado</h4>
														<p>Nuestro software guardará tu inventario personalizado. Así siempore podrás saber como va tu huella verde.
														Lo único que tienes que hacer es buscar en nuestra base de datos o capturar tu mismo tus equipos electrónicos o eléctricos
														Con esto podrás indicar cuantas horas los usas y el software te dará el resultado.
														!Facilísimo!</p>
													</div>
												</div>
											</div>
										</div>
										<!--<div class="section-top-border">
											<h3 class="mb-30">Block Quotes</h3>
											<div class="row">
												<div class="col-lg-12">
													<blockquote class="generic-blockquote">
														“Recently, the US Federal government banned online casinos from operating in America by
														making it illegal to
														transfer money to them through any US bank or payment system. As a result of this law, most
														of the popular
														online casino networks such as Party Gaming and PlayTech left the United States. Overnight,
														online casino
														players found themselves being chased by the Federal government. But, after a fortnight, the
														online casino
														industry came up with a solution and new online casinos started taking root. These began to
														operate under a
														different business umbrella, and by doing that, rendered the transfer of money to and from
														them legal. A major
														part of this was enlisting electronic banking systems that would accept this new
														clarification and start doing
														business with me. Listed in this article are the electronic banking”
													</blockquote>
												</div>
											</div>
										</div>-->
										<div class="section-top-border">
											<h3 class="mb-30">Wiki Verde <?php if($nombre!=NULL){ echo '<br>Inventario personalizado de '.$nombre; } ?></h3>
											<div class="progress-table-wrap">
												<div class="progress-table">
													<div class="table-head">
														<div class="Nombrewiky">Modelo</div>
														<div class="country"></div>
														<div class="visit">Watts</div>
														<div class="visit">CO<sub>2</sub></div>
														<div class="percentage">Consumo de energía</div>
														<div class="serial">Seleccionar</div>
													</div>
													<?php
													include ('include/conn.php');
                                                    $consultproducto = "SELECT * FROM `productos`";
                                                    if($nombre!=NULL){
                                                        $consultproducto= "SELECT * FROM productos INNER JOIN rel_user_product ON productos.ID = rel_user_product.productos_ID WHERE rel_user_product.usuarios_ID = $usuario_ID" ; 
                                                    } 
                                                        $resultproducto=mysqli_query($conn,$consultproducto); 
                                                        foreach($resultproducto as $row){
                                                            $nomproductos = utf8_encode($row['name']);
                                                            $img = $row['photo'];
                                                            $w = $row['w'];
                                                            $eficiencia = $row['porcentaje'];
                                                            $resultw = ($w/1000) * 0.41;
                                                    ?>
                                                    <div class="table-row">
                                                    	<div class="Nombrewiky"><? echo $nomproductos; ?></div>
                                                    	<div class="country"> <img src="assets/img/elements/<? echo $img; ?>" alt="flag"></div>
                                                    	<div class="visit"><? echo number_format($w, 2, '.', ''); ?> <em>watts</em></div>
                                                    	<div class="visit"><? echo number_format($resultw, 4, '.', ''); ?> kg/h</div>
                                                    		<div class="percentage">
                                                    			<div class="progress">
                                                    		    <div class="progress-bar color-1" role="progressbar" style="width: <?=$eficiencia?>%" aria-valuenow="<?=$eficiencia?>" aria-valuemin="0" aria-valuemax="<?=$eficiencia?>"><?=$eficiencia?>%</div>
                                                    			</div>
                                                    	    </div>
                                                    		<div class="serial"><input type="checkbox" id="vehicle1" name="vehicle1" value=""></div>
                                                    </div>
                                                    <?php
                                                        }
                                                    ?><p><br></p>
                                                    <h3 class="mb-30">Comparativo entre Impresoras</h3>
                                                    
                                                    <?php
													include ('include/conn.php');
													 $consultproducto2 = "SELECT * FROM `productos` WHERE `type` = 'Impresora' ORDER BY `w` DESC ";
													if ($nombre=NULL){
                                                    	$consultproducto2= "SELECT * FROM productos INNER JOIN rel_user_product ON productos.ID = rel_user_product.productos_ID WHERE rel_user_product.usuarios_ID = $usuario_ID " ; 
                                                    }
                                                         
                                                         $resultproducto2=mysqli_query($conn,$consultproducto2); 
                                                        foreach($resultproducto2 as $row){
                                                            $nomproductos = utf8_encode($row['name']);
                                                            $img = $row['photo'];
                                                            $w = $row['w'];
                                                            $eficiencia = $row['porcentaje'];
                                                            $resultw = ($w/1000) * 0.41;
                                                            $maseficiente = ((400/1000) * 0.015) * 8;
                                                            $cost = ((($w/1000) * 0.015) * 8) - $maseficiente;
                                                            $eficiente = $maseficiente /  $cost;
                                                            
                                                            ?>
                                                    <div class="table-row">
                                                    	<div class="Nombrewiky"><? echo $nomproductos; ?></div>
                                                    	<div class="country"> <img src="assets/img/elements/<? echo $img; ?>" alt="flag"></div>
                                                    	<div class="visit">Gasta: $<? echo number_format($cost, 2, '.', ''); ?>MXN/día</div>
                                                    	<div class="visit"><? echo number_format($resultw, 4, '.', ''); ?> kg/h</div>
                                                    		<div class="percentage">
                                                    			<div class="progress">
                                                    		    <div class="progress-bar color-1" role="progressbar" style="width: <?=$eficiente?>%" aria-valuenow="<?=$eficiente?>" aria-valuemin="0" aria-valuemax="<?=$eficiente?>"><?=$eficiente?>%</div>
                                                    			</div>
                                                    	    </div>
                                                    		<div class="serial"><input type="checkbox" id="vehicle1" name="vehicle1" value=""></div>
                                                    </div>
                                                    <?php
                                                        }
                                                    ?><p><br></p>         
                                                            
                                                    <div class="section-top-border">
											<h3 class="mb-30">Wiki Verde Autos</h3>
											<div class="progress-table-wrap">
												<div class="progress-table">
													<div class="table-head">
														<div class="Nombrewiky">Modelo</div>
														<div class="country"></div>
														<div class="visit">Rendimiento</div>
														<div class="visit">CO<sub>2</sub></div>
														<div class="percentage">Eficiencia</div>
														<div class="serial">Seleccionar</div>
													</div>
													</div>
											</div>
													<?php
													
                                                    $consultauto = "SELECT * FROM `autos`";
                                                    if($nombre!=NULL){
                                                        $consultproducto= "SELECT * FROM autos INNER JOIN rel_user_autos ON autos.ID = rel_user_autos.autos_ID WHERE rel_user_autos.usuarios_ID = $usuario_ID" ; 
                                                    } 

                                                        $resultauto=mysqli_query($conn,$consultauto); 
                                                        foreach($resultauto as $row){
                                                            $nomproductos = utf8_encode($row['marca']);
                                                            $nomauto = utf8_encode($row['modelo']);
                                                            $combustible = $row['tipo_gas'];
                                                            $rendimiento = $row['rendimiento'];
                                                            $eficiencia = $row['porcentaje'];
                                                            $img = $row['photo'];
                                                            $w = $row['rendimiento'];
                                                            $factormagna = 2.334;
                                                            $factorpremium = 2.329;
                                                            $factordiesel = 2.579;
                                                            $factorturbosina = 2.505;

if($combustible=='Premium'){
    $factorgasolina = $factorpremium;
}
if($combustible=='Magna'){
    $factorgasolina = $factormagna;
}
if($combustible=='Diesel'){
    $factorgasolina = $factordiesel;
}
                                                            $resultw = ($factorgasolina / $rendimiento);
                                                            if($rendimiento==0.000){
                                                                $resultw = 0;
                                                            }
                                                    ?>
                                                    <div class="table-row">
                                                    	<div class="Nombrewiky"><? echo $nomproductos.' '.$nomauto; ?></div>
                                                    	<div class="country"> <img src="assets/img/elements/<? echo $img; ?>" alt="flag"></div>
                                                    	<div class="visit"><? echo $w; ?> </div>
                                                    	<div class="visit"><? echo number_format($resultw, 4, '.', '');; ?> kg/h</div>
                                                    		<div class="percentage">
                                                    			<div class="progress">
                                                    		    <div class="progress-bar color-1" role="progressbar" style="width: <?=$eficiencia?>%" aria-valuenow="<?=$eficiencia?>" aria-valuemin="0" aria-valuemax="<?=$eficiencia?>"><?=$eficiencia?>%</div>
                                                    			</div>
                                                    	    </div>
                                                    	    <div class="primary-checkbox">
																<input type="checkbox" id="vehicle1"name="vehicle1" value="">
																<label for="default-checkbox"></label>
															</div>
                                                    		<div class="serial"><input type="checkbox" id="vehicle1" name="vehicle1" value=""></div>
                                                    </div>
                                                    <?php
                                                        }
                                                    ?>
													
													
												
												</div>
											</div>
										</div>
										<!--div class="section-top-border">
											<h3>Image Gallery</h3>
											<div class="row gallery-item">
												<div class="col-md-4">
													<a href="assets/img/elements/g1.jpg" class="img-pop-up">
														<div class="single-gallery-image" style="background: url(assets/img/elements/g1.jpg);"></div>
													</a>
												</div>
												<div class="col-md-4">
													<a href="assets/img/elements/g2.jpg" class="img-pop-up">
														<div class="single-gallery-image" style="background: url(assets/img/elements/g2.jpg);"></div>
													</a>
												</div>
												<div class="col-md-4">
													<a href="assets/img/elements/g3.jpg" class="img-pop-up">
														<div class="single-gallery-image" style="background: url(assets/img/elements/g3.jpg);"></div>
													</a>
												</div>
												<div class="col-md-6">
													<a href="assets/img/elements/g4.jpg" class="img-pop-up">
														<div class="single-gallery-image" style="background: url(assets/img/elements/g4.jpg);"></div>
													</a>
												</div>
												<div class="col-md-6">
													<a href="assets/img/elements/g5.jpg" class="img-pop-up">
														<div class="single-gallery-image" style="background: url(assets/img/elements/g5.jpg);"></div>
													</a>
												</div>
												<div class="col-md-4">
													<a href="assets/img/elements/g6.jpg" class="img-pop-up">
														<div class="single-gallery-image" style="background: url(assets/img/elements/g6.jpg);"></div>
													</a>
												</div>
												<div class="col-md-4">
													<a href="assets/img/elements/g7.jpg" class="img-pop-up">
														<div class="single-gallery-image" style="background: url(assets/img/elements/g7.jpg);"></div>
													</a>
												</div>
												<div class="col-md-4">
													<a href="assets/img/elements/g8.jpg" class="img-pop-up">
														<div class="single-gallery-image" style="background: url(assets/img/elements/g8.jpg);"></div>
													</a>
												</div>
											</div>
										</div>
										<div class="section-top-border">
											<div class="row">
												<div class="col-md-4">
													<h3 class="mb-20">Image Gallery</h3>
													<div class="typography">
														<h1>This is header 01</h1>
														<h2>This is header 02</h2>
														<h3>This is header 03</h3>
														<h4>This is header 04</h4>
														<h5>This is header 01</h5>
														<h6>This is header 01</h6>
													</div>
												</div>
												<div class="col-md-4 mt-sm-30">
													<h3 class="mb-20">Unordered List</h3>
													<div class="">
														<ul class="unordered-list">
															<li>Fta Keys</li>
															<li>For Women Only Your Computer Usage</li>
															<li>Facts Why Inkjet Printing Is Very Appealing
																<ul>
																	<li>Addiction When Gambling Becomes
																		<ul>
																			<li>Protective Preventative Maintenance</li>
																		</ul>
																	</li>
																</ul>
															</li>
															<li>Dealing With Technical Support 10 Useful Tips</li>
															<li>Make Myspace Your Best Designed Space</li>
															<li>Cleaning And Organizing Your Computer</li>
														</ul>
													</div>
												</div>
												<div class="col-md-4 mt-sm-30">
													<h3 class="mb-20">Ordered List</h3>
													<div class="">
														<ol class="ordered-list">
															<li><span>Fta Keys</span></li>
															<li><span>For Women Only Your Computer Usage</span></li>
															<li><span>Facts Why Inkjet Printing Is Very Appealing</span>
																<ol class="ordered-list-alpha">
																	<li><span>Addiction When Gambling Becomes</span>
																		<ol class="ordered-list-roman">
																			<li><span>Protective Preventative Maintenance</span></li>
																		</ol>
																	</li>
																</ol>
															</li>
															<li><span>Dealing With Technical Support 10 Useful Tips</span></li>
															<li><span>Make Myspace Your Best Designed Space</span></li>
															<li><span>Cleaning And Organizing Your Computer</span></li>
														</ol>
													</div>
												</div>
											</div>
										</div>
										<div class="section-top-border">
											<div class="row">
												<div class="col-lg-8 col-md-8">
													<h3 class="mb-30">Form Element</h3>
													<form action="#">
														<div class="mt-10">
															<input type="text" name="first_name" placeholder="First Name"
															onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'" required
															class="single-input">
														</div>
														<div class="mt-10">
															<input type="text" name="last_name" placeholder="Last Name"
															onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'" required
															class="single-input">
														</div>
														<div class="mt-10">
															<input type="text" name="last_name" placeholder="Last Name"
															onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'" required
															class="single-input">
														</div>
														<div class="mt-10">
															<input type="email" name="EMAIL" placeholder="Email address"
															onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email address'" required
															class="single-input">
														</div>
														<div class="input-group-icon mt-10">
															<div class="icon"><i class="fa fa-thumb-tack" aria-hidden="true"></i></div>
															<input type="text" name="address" placeholder="Address" onfocus="this.placeholder = ''"
															onblur="this.placeholder = 'Address'" required class="single-input">
														</div>
														<div class="input-group-icon mt-10">
															<div class="icon"><i class="fa fa-plane" aria-hidden="true"></i></div>
															<div class="form-select" id="default-select"">
																<select>
																	<option value=" 1">City</option>
																	<option value="1">Dhaka</option>
																	<option value="1">Dilli</option>
																	<option value="1">Newyork</option>
																	<option value="1">Islamabad</option>
																</select>
															</div>
														</div>
														<div class="input-group-icon mt-10">
															<div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>
															<div class="form-select" id="default-select"">
																<select>
																	<option value=" 1">Country</option>
																	<option value="1">Bangladesh</option>
																	<option value="1">India</option>
																	<option value="1">England</option>
																	<option value="1">Srilanka</option>
																</select>
															</div>
														</div>
														
														<div class="mt-10">
															<textarea class="single-textarea" placeholder="Message" onfocus="this.placeholder = ''"
															onblur="this.placeholder = 'Message'" required></textarea>
														</div>
														<div class="mt-10">
															<input type="text" name="first_name" placeholder="Primary color"
															onfocus="this.placeholder = ''" onblur="this.placeholder = 'Primary color'" required
															class="single-input-primary">
														</div>
														<div class="mt-10">
															<input type="text" name="first_name" placeholder="Accent color"
															onfocus="this.placeholder = ''" onblur="this.placeholder = 'Accent color'" required
															class="single-input-accent">
														</div>
														<div class="mt-10">
															<input type="text" name="first_name" placeholder="Secondary color"
															onfocus="this.placeholder = ''" onblur="this.placeholder = 'Secondary color'"
															required class="single-input-secondary">
														</div>
													</form>
												</div>
												<div class="col-lg-3 col-md-4 mt-sm-30">
													<div class="single-element-widget">
														<h3 class="mb-30">Switches</h3>
														<div class="switch-wrap d-flex justify-content-between">
															<p>01. Sample Switch</p>
															<div class="primary-switch">
																<input type="checkbox" id="default-switch">
																<label for="default-switch"></label>
															</div>
														</div>
														<div class="switch-wrap d-flex justify-content-between">
															<p>02. Primary Color Switch</p>
															<div class="primary-switch">
																<input type="checkbox" id="primary-switch" checked>
																<label for="primary-switch"></label>
															</div>
														</div>
														<div class="switch-wrap d-flex justify-content-between">
															<p>03. Confirm Color Switch</p>
															<div class="confirm-switch">
																<input type="checkbox" id="confirm-switch" checked>
																<label for="confirm-switch"></label>
															</div>
														</div>
													</div>
													<div class="single-element-widget mt-30">
														<h3 class="mb-30">Selectboxes</h3>
														<div class="default-select" id="default-select"">
															<select>
																<option value=" 1">English</option>
																<option value="1">Spanish</option>
																<option value="1">Arabic</option>
																<option value="1">Portuguise</option>
																<option value="1">Bengali</option>
															</select>
														</div>
													</div>
													<div class="single-element-widget mt-30">
														<h3 class="mb-30">Checkboxes</h3>
														<div class="switch-wrap d-flex justify-content-between">
															<p>01. Sample Checkbox</p>
															<div class="primary-checkbox">
																<input type="checkbox" id="default-checkbox">
																<label for="default-checkbox"></label>
															</div>
														</div>
														<div class="switch-wrap d-flex justify-content-between">
															<p>02. Primary Color Checkbox</p>
															<div class="primary-checkbox">
																<input type="checkbox" id="primary-checkbox" checked>
																<label for="primary-checkbox"></label>
															</div>
														</div>
														<div class="switch-wrap d-flex justify-content-between">
															<p>03. Confirm Color Checkbox</p>
															<div class="confirm-checkbox">
																<input type="checkbox" id="confirm-checkbox">
																<label for="confirm-checkbox"></label>
															</div>
														</div>
														<div class="switch-wrap d-flex justify-content-between">
															<p>04. Disabled Checkbox</p>
															<div class="disabled-checkbox">
																<input type="checkbox" id="disabled-checkbox" disabled>
																<label for="disabled-checkbox"></label>
															</div>
														</div>
														<div class="switch-wrap d-flex justify-content-between">
															<p>05. Disabled Checkbox active</p>
															<div class="disabled-checkbox">
																<input type="checkbox" id="disabled-checkbox-active" checked disabled>
																<label for="disabled-checkbox-active"></label>
															</div>
														</div>
													</div>
													<div class="single-element-widget mt-30">
														<h3 class="mb-30">Radios</h3>
														<div class="switch-wrap d-flex justify-content-between">
															<p>01. Sample radio</p>
															<div class="primary-radio">
																<input type="checkbox" id="default-radio">
																<label for="default-radio"></label>
															</div>
														</div>
														<div class="switch-wrap d-flex justify-content-between">
															<p>02. Primary Color radio</p>
															<div class="primary-radio">
																<input type="checkbox" id="primary-radio" checked>
																<label for="primary-radio"></label>
															</div>
														</div>
														<div class="switch-wrap d-flex justify-content-between">
															<p>03. Confirm Color radio</p>
															<div class="confirm-radio">
																<input type="checkbox" id="confirm-radio" checked>
																<label for="confirm-radio"></label>
															</div>
														</div>
														<div class="switch-wrap d-flex justify-content-between">
															<p>04. Disabled radio</p>
															<div class="disabled-radio">
																<input type="checkbox" id="disabled-radio" disabled>
																<label for="disabled-radio"></label>
															</div>
														</div>
														<div class="switch-wrap d-flex justify-content-between">
															<p>05. Disabled radio active</p>
															<div class="disabled-radio">
																<input type="checkbox" id="disabled-radio-active" checked disabled>
																<label for="disabled-radio-active"></label>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div -->
								<!-- End Align Area -->
							</main>
                                    <?php include('include/footer.php');?>
								<!-- Scroll Up -->
								<div id="back-top" >
									<a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
								</div>
								<!-- JS here -->

								<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
								<!-- Jquery, Popper, Bootstrap -->
								<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
								<script src="./assets/js/popper.min.js"></script>
								<script src="./assets/js/bootstrap.min.js"></script>
								<!-- Jquery Mobile Menu -->
								<script src="./assets/js/jquery.slicknav.min.js"></script>

								<!-- Jquery Slick , Owl-Carousel Plugins -->
								<script src="./assets/js/owl.carousel.min.js"></script>
								<script src="./assets/js/slick.min.js"></script>
								<!-- One Page, Animated-HeadLin -->
								<script src="./assets/js/wow.min.js"></script>
								<script src="./assets/js/animated.headline.js"></script>
								<script src="./assets/js/jquery.magnific-popup.js"></script>

								<!-- Date Picker -->
								<script src="./assets/js/gijgo.min.js"></script>
								<!-- Nice-select, sticky -->
								<script src="./assets/js/jquery.nice-select.min.js"></script>
								<script src="./assets/js/jquery.sticky.js"></script>
								
								<!-- counter , waypoint,Hover Direction -->
								<script src="./assets/js/jquery.counterup.min.js"></script>
								<script src="./assets/js/waypoints.min.js"></script>
								<script src="./assets/js/jquery.countdown.min.js"></script>
								<script src="./assets/js/hover-direction-snake.min.js"></script>

								<!-- contact js -->
								<script src="./assets/js/contact.js"></script>
								<script src="./assets/js/jquery.form.js"></script>
								<script src="./assets/js/jquery.validate.min.js"></script>
								<script src="./assets/js/mail-script.js"></script>
								<script src="./assets/js/jquery.ajaxchimp.min.js"></script>
								
								<!-- Jquery Plugins, main Jquery -->	
								<script src="./assets/js/plugins.js"></script>
								<script src="./assets/js/main.js"></script>
								
							</body>
							</html>