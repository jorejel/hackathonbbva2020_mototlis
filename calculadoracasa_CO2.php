<!doctype html>
<html class="no-js" lang="zxx">
<head><meta charset="gb18030">
	
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Calculadora Casa</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

	<!-- CSS here -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="assets/css/slicknav.css">
	<link rel="stylesheet" href="assets/css/animate.min.css">
	<link rel="stylesheet" href="assets/css/hamburgers.min.css">
	<link rel="stylesheet" href="assets/css/magnific-popup.css">
	<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="assets/css/themify-icons.css">
	<link rel="stylesheet" href="assets/css/slick.css">
	<link rel="stylesheet" href="assets/css/nice-select.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<!--? Preloader Start -->
	<div id="preloader-active">
		<div class="preloader d-flex align-items-center justify-content-center">
			<div class="preloader-inner position-relative">
				<div class="preloader-circle"></div>
				<div class="preloader-img pere-text">
					<img src="assets/img/logo/loder.png" alt="">
				</div>
			</div>
		</div>
	</div>
	<!-- Preloader Start -->
	<header>
		<!-- Header Start -->
            <?php include('include/menu.php');?>

		<!-- Header End -->
	</header>
	<main>
		<!--? Hero Start -->
		<div class="slider-area2">
			<div class="slider-height2 d-flex align-items-center">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="hero-cap hero-cap2 pt-70">
								<h2>Calculadora de emisiones CO<sub>2</sub></h2>
								<nav aria-label="breadcrumb">
									<ol class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.php">Inicio</a></li>
										<li class="breadcrumb-item"><a href="#">Calculadora de CO2</a></li>
									</ol>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Hero End -->
		<!--? Start Sample Area -->
		<section class="sample-text-area">
			<div class="container box_1170">
				<div class="co2Form">
					<h1 align="center" class="mb-4">Consumo de energ&iacute;a mensual en casa.</h1>
					<p class="sample-text mb-5" align="center">
					Escribe cuanto consumes al mes en gas y en luz. Obten la informac&iacute;on de tus recibos.<br>
					Recuerda que algunos recibos son bimestrales, para que no la vayas a cajetear.
					</p>
					<p class="h5 mb-4" align="center">&#191;Qu&eacute; tipo de gas tienes?</p>
					<div class="d-flex justify-content-center">
						<div onclick="tipoGas(3)" align="center" class="gasHover mx-4">
							<img src="assets/img/icono/gas-natural.png" alt="" width="180px">
							<p>Gas Natural (m<sup>3</sup>)</p>
						</div>
						<div onclick="tipoGas(1)" align="center" class="gasHover mx-4">
							<img src="assets/img/icono/gasLPcilindro.png" alt="" width="180px">
							<p>Gas LP <br>Cilindro</p>
						</div>
						<div onclick="tipoGas(2)" align="center" class="gasHover mx-4">
							<img src="assets/img/icono/gasLPtanque.png" alt="" width="180px">
							<p>Gas LP <br>Tanque estacionario</p>
						</div>
					</div>
					<form id="formCalcuEmi" class="formCalcuEmi" novalidate method="post">
						<div class="formGasLPcilindro mb-3" style="display:none" align="center">
							<label for="GasLPcilindro">Gas LP comprado en cilindro (kg)</label>
							<input type="number" id="GasLPcilindro" name="lp" size="5" placeholder="Ejemplo: 50" min="0"  class="form-control">
						</div>
						<div class="formGasLPtanque mb-3" style="display:none" align="center">
							<label for="GasLPtanque">Gas LP en tanque estacionario (L)</label>
							<input type="number" id="GasLPtanque" name="lpliq" size="5" placeholder="Ejemplo: 25" min="0"  class="form-control">
						</div>
						<div class="formGasNatural mb-3" style="display:none" align="center">
							<label for="GasNatural">Gas Natural (m<sup>3</sup>)</label>
							<input type="number" id="GasNatural" name="gn" size="5" placeholder="Ejemplo: 15" min="0"  class="form-control">
						</div>
						<div class="formGenera mb-3l" style="display:none" align="center">
							<label for="kw">Energ&iacute;a electrica (kWh)</label>
							<input type="number" id="kw" name="kw" size="5" placeholder="Ejemplo: 0" min="0"  class="form-control">
							<input type="reset"  class="genric-btn default circle my-4" value="Borrar" name="B2">
							<buttom class="genric-btn success  circle my-4" onclick="calculoEmision()">Calcular</buttom>
						</div>
					</form>
				</div>
				<div class="formResultado" style="display:none" align="center">
					<h1 class="display-2">Tus resultados</h1>
					<div class="result1"></div>
					<p>Tu consumo de kWh produce = <b class="result2"></b> kg de CO<sup>2</sup></p>
					<p>Tu aportacion de contaminaci&oacute;n mensual de CO<sub>2</sub> al planeta es de = <b class="result3"></b> kg de CO<sub>2</sub></p>
					<buttom class="genric-btn success e-large circle my-4" onclick="nuevoCalculo()">Nuevo calculo</buttom>
				</div>

<style>
	.gasHover:hover {
		opacity: 0.4;
	}
</style>

<script type="text/javascript">
	function nuevoCalculo(){
		$('.co2Form').show()
		$('.formResultado').hide()	
		$('.formGasLPcilindro').hide()
		$('.formGasLPtanque').hide()
		$('.formGasNatural').hide()
		$('.formGenera').hide()
		


	}
    function calculoEmision(){
      var forms = document.getElementsByClassName('formCalcuEmi');
      var validation = Array.prototype.filter.call(forms, function(form) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }else{
        var para= new FormData($("#formCalcuEmi")[0]);
        $.ajax({
          url: 'assets/ajax/calculadora.php?calculoEmision=calculoEmision',
          type: 'POST',
          data: para,
          dataType:"json",
          contentType: false,
          processData: false,
          cache: false,
        }).done(function(data) {
          //if (data.estatus===1) {
			$("#formCalcuEmi")[0].reset();
			$('.co2Form').hide()
			$('.formResultado').show()
			$('.result1').html(data.result1)
			$('.result2').html(data.result2)
			$('.result3').html(data.result3)
          //}
        }).fail(function(jqXHR, textStatus, errorThrown) {
          console.log("error"+jqXHR.responseText);
        });
      }
      form.classList.add('was-validated');
      });
    }



	function tipoGas(value){
      if(value===1){
		$('.formGasLPcilindro').toggle();
		$('.formGasLPtanque').hide();
		$('.formGasNatural').hide();
		$('.formGenera').show();
		$('#GasLPcilindro').val('');
		$('#GasLPtanque').val('');
		$('#GasNatural').val('');
		$('#kw').val('');
      }
      if(value===2){
		$('.formGasLPtanque').toggle();
		$('.formGasLPcilindro').hide();
		$('.formGasNatural').hide();
		$('.formGenera').show();
		$('#GasLPcilindro').val('');
		$('#GasLPtanque').val('');
		$('#GasNatural').val('');
		$('#kw').val('');
	  }
	  if(value===3){
		$('.formGasNatural').toggle();
		$('.formGasLPtanque').hide();
		$('.formGasLPcilindro').hide();
		$('.formGenera').show();
		$('#GasLPcilindro').val('');
		$('#GasLPtanque').val('');
		$('#GasNatural').val('');
		$('#kw').val('');
      }
	}
</script>
















			</div>
		</section>

							</main>
            <?php include('include/footer.php');?>
								<!-- Scroll Up -->
								<div id="back-top" >
									<a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
								</div>
								<!-- JS here -->

								<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
								<!-- Jquery, Popper, Bootstrap -->
								<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
								<script src="./assets/js/popper.min.js"></script>
								<script src="./assets/js/bootstrap.min.js"></script>
								<!-- Jquery Mobile Menu -->
								<script src="./assets/js/jquery.slicknav.min.js"></script>

								<!-- Jquery Slick , Owl-Carousel Plugins -->
								<script src="./assets/js/owl.carousel.min.js"></script>
								<script src="./assets/js/slick.min.js"></script>
								<!-- One Page, Animated-HeadLin -->
								<script src="./assets/js/wow.min.js"></script>
								<script src="./assets/js/animated.headline.js"></script>
								<script src="./assets/js/jquery.magnific-popup.js"></script>

								<!-- Date Picker -->
								<script src="./assets/js/gijgo.min.js"></script>
								<!-- Nice-select, sticky -->
								<script src="./assets/js/jquery.nice-select.min.js"></script>
								<script src="./assets/js/jquery.sticky.js"></script>
								
								<!-- counter , waypoint,Hover Direction -->
								<script src="./assets/js/jquery.counterup.min.js"></script>
								<script src="./assets/js/waypoints.min.js"></script>
								<script src="./assets/js/jquery.countdown.min.js"></script>
								<script src="./assets/js/hover-direction-snake.min.js"></script>

								<!-- contact js -->
								<script src="./assets/js/contact.js"></script>
								<script src="./assets/js/jquery.form.js"></script>
								<script src="./assets/js/jquery.validate.min.js"></script>
								<script src="./assets/js/mail-script.js"></script>
								<script src="./assets/js/jquery.ajaxchimp.min.js"></script>
								
								<!-- Jquery Plugins, main Jquery -->	
								<script src="./assets/js/plugins.js"></script>
								<script src="./assets/js/main.js"></script>
								
							</body>
							</html>