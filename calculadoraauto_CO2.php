<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Calculadora Automovil</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

	<!-- CSS here -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="assets/css/slicknav.css">
	<link rel="stylesheet" href="assets/css/animate.min.css">
	<link rel="stylesheet" href="assets/css/hamburgers.min.css">
	<link rel="stylesheet" href="assets/css/magnific-popup.css">
	<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="assets/css/themify-icons.css">
	<link rel="stylesheet" href="assets/css/slick.css">
	<link rel="stylesheet" href="assets/css/nice-select.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<!--? Preloader Start -->
	<div id="preloader-active">
		<div class="preloader d-flex align-items-center justify-content-center">
			<div class="preloader-inner position-relative">
				<div class="preloader-circle"></div>
				<div class="preloader-img pere-text">
					<img src="assets/img/logo/loder.png" alt="">
				</div>
			</div>
		</div>
	</div>
	<!-- Preloader Start -->
	<header>
		<!-- Header Start -->
            <?php include('include/menu.php');?>

		<!-- Header End -->
	</header>
	<main>
		<!--? Hero Start -->
		<div class="slider-area2">
			<div class="slider-height2 d-flex align-items-center">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="hero-cap hero-cap2 pt-70">
								<h2>Calculadora de emisión de CO<sub>2</sub> para automóvil</h2>
								<nav aria-label="breadcrumb">
									<ol class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html">Inicio</a></li>
										<li class="breadcrumb-item"><a href="#">Calculadora CO2 auto</a></li>
									</ol>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Hero End -->
		<!--? Start Sample Area -->
		<section class="sample-text-area">
			<div class="container box_1170">
				<div class="co2Form">
					<h1 align="center" class="mb-4">Como puedo saber cual es la huella de CO<sub>2</sub> de mi auto.</h1>
					<p class="sample-text mb-5" align="center">
					    Es muy sencillo, escribe cual es el rendmiento en km/L de tu auto asi como el tipo de combustible que usa<br>
                        De esa forma podemos saber cuanto CO<sub>2</sub> estas generando.
                    </p>
                    <form id="formCalcuEmi" class="formCalcuEmi" novalidate method="post" align="center">
                        <p class="h5 mb-4" align="center">&#191;Qu&eacute; usas?</p>
                        <div class="d-flex justify-content-center">
                            <div class="hide_button form-check form-check-inline">
                            <input class="form-check-input input_perso" type="radio" id="magna" name="combustible" value="magna" required>
                            <label class="form-check-label" for="magna" align="center">
                                <img src="assets/img/icono/magna.png" width="120"><br><b>Magna</b>
                            </label>
                            </div>
                            <div class="hide_button form-check form-check-inline">
                            <input class="form-check-input input_perso" type="radio" id="diesel" name="combustible" value="diesel" required>
                            <label class="form-check-label" for="diesel" align="center">
                                <img src="assets/img/icono/diesel.png" width="120"><br><b>Diesel</b>
                            </label>
                            </div>
                            <div class="hide_button form-check form-check-inline">
                            <input class="form-check-input input_perso" type="radio" id="premium" name="combustible" value="premium" required>
                            <label class="form-check-label" for="premium" align="center">
                                <img src="assets/img/icono/premium.png" width="120"><br><b>Premium</b>
                            </label>
                            </div>
                            <div class="hide_button form-check form-check-inline">
                            <input class="form-check-input input_perso" type="radio" id="electric" name="combustible" value="electric" required>
                            <label class="form-check-label" for="electric" align="center">
                                <img src="assets/img/icono/carga.png" width="120"><br><b>Eléctrico</b>
                            </label>
                            </div>
                        </div>
					
                        <label for="rendimiento">Rendmiento (km/L)</label>
                        <input type="number" id="rendimiento" name="rendimiento" size="5" placeholder="Ejemplo: 50" min="0"  class="form-control" required>
                        <label for="recorrido">Cuantos km recorres</label>
                        <input type="number" id="recorrido" name="recorrido" size="5" placeholder="Ejemplo: 50" min="0"  class="form-control" required>
                        
                        <input type="reset"  class="genric-btn default circle my-4" value="Borrar" name="B2">
                        <buttom class="genric-btn success  circle my-4" onclick="calculoAuto()">Calcular</buttom>
					</form>
				</div>
				<div class="formResultado" style="display:none" align="center">
					<h1 class="display-2">Tus resultados</h1>
					<div class="result1"></div>
                    <p>Tu aportación contaminante de CO<sub>2</sub> para al planeta recorriendo <b class="result2"></b> km es de = <b class="result3"></b> kg de CO<sub>2</sub></p>
					<buttom class="genric-btn success e-large circle my-4" onclick="nuevoCalculo()">Nuevo cálculo</buttom>
				</div>

<style>
    .hide_button .input_perso {
      display: none;
    }
    .input_perso:checked + label {
        border: 2px solid red;
    }
</style>

<script type="text/javascript">
	function nuevoCalculo(){
		$('.co2Form').show()
		$('.formResultado').hide()	
	}
    function calculoAuto(){
      var forms = document.getElementsByClassName('formCalcuEmi');
      var validation = Array.prototype.filter.call(forms, function(form) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }else{
        var para= new FormData($("#formCalcuEmi")[0]);
        $.ajax({
          url: 'assets/ajax/calculadora.php?calculoAuto=calculoAuto',
          type: 'POST',
          data: para,
          dataType:"json",
          contentType: false,
          processData: false,
          cache: false,
        }).done(function(data) {
          //if (data.estatus===1) {
          console.log(data)
			$("#formCalcuEmi")[0].reset();
			$('.co2Form').hide()
			$('.formResultado').show()
			$('.result1').html(data.result1)
            $('.result2').html(data.result2)
            $('.result3').html(data.result3)
          //}
        }).fail(function(jqXHR, textStatus, errorThrown) {
          console.log("error"+jqXHR.responseText);
        });
      }
      form.classList.add('was-validated');
      });
    }
</script>
















			</div>
		</section>

							</main>



<?php include('include/footer.php');?>

								<!-- Scroll Up -->
								<div id="back-top" >
									<a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
								</div>
								<!-- JS here -->

								<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
								<!-- Jquery, Popper, Bootstrap -->
								<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
								<script src="./assets/js/popper.min.js"></script>
								<script src="./assets/js/bootstrap.min.js"></script>
								<!-- Jquery Mobile Menu -->
								<script src="./assets/js/jquery.slicknav.min.js"></script>

								<!-- Jquery Slick , Owl-Carousel Plugins -->
								<script src="./assets/js/owl.carousel.min.js"></script>
								<script src="./assets/js/slick.min.js"></script>
								<!-- One Page, Animated-HeadLin -->
								<script src="./assets/js/wow.min.js"></script>
								<script src="./assets/js/animated.headline.js"></script>
								<script src="./assets/js/jquery.magnific-popup.js"></script>

								<!-- Date Picker -->
								<script src="./assets/js/gijgo.min.js"></script>
								<!-- Nice-select, sticky -->
								<script src="./assets/js/jquery.nice-select.min.js"></script>
								<script src="./assets/js/jquery.sticky.js"></script>
								
								<!-- counter , waypoint,Hover Direction -->
								<script src="./assets/js/jquery.counterup.min.js"></script>
								<script src="./assets/js/waypoints.min.js"></script>
								<script src="./assets/js/jquery.countdown.min.js"></script>
								<script src="./assets/js/hover-direction-snake.min.js"></script>

								<!-- contact js -->
								<script src="./assets/js/contact.js"></script>
								<script src="./assets/js/jquery.form.js"></script>
								<script src="./assets/js/jquery.validate.min.js"></script>
								<script src="./assets/js/mail-script.js"></script>
								<script src="./assets/js/jquery.ajaxchimp.min.js"></script>
								
								<!-- Jquery Plugins, main Jquery -->	
								<script src="./assets/js/plugins.js"></script>
								<script src="./assets/js/main.js"></script>
								
							</body>
							</html>