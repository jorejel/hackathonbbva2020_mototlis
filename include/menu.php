    <header><meta charset="gb18030">
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header ">
                <div class="header-bottom  header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <!-- Logo -->
                            <div class="col-xl-2 col-lg-2">
                                <div class="logo">
                                    <a href="index.php"><img src="huella_xoxoktli.png" class="img-fluid" alt="xoxoktli"></a>
                                </div>
                            </div>
                            <div class="col-xl-10 col-lg-10">
                                <div class="menu-wrapper  d-flex align-items-center justify-content-end">
                                    <!-- Main-menu -->
                                    <div class="main-menu d-none d-lg-block">
                                        <nav>
                                            <ul id="navigation">                                                                                          
                                                <li><a href="index.php">Inicio</a></li>
                                                <li><a href="proyecto.php">Proyecto</a></li>
                                                <li><a href="equipo.php">El equipo</a></li>
                                                <li><a href="wikiverde.php">Wiki Verde</a></li>
                                                <li>Calculadora CO<sub>2</sub></a>
                                                    <ul class="submenu">
                                                        <li><a href="login.php">Iniciar sesión</a></li>
                                                        <li><a href="calculadoracasa_CO2.php">Casa</a></li>
                                                        <li><a href="calculadoraauto_CO2.php">Automóvil</a></li>
                                                        <li><a href="oficina.php">Oficina</a></li>
                                                        <li><a href="inteligente.php">Inteligente</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="contacto.php">Contacto</a></li>
                                                </ul>
                                        </nav>
                                    </div>
                                    <!-- Header-btn -->
                                    <div class="header-right-btn d-none d-lg-block ml-20">
                                        <a href="login.php" class="btn header-btn">Iniciar sesión</a>
                                    </div>
                                </div>
                            </div> 
                            <!-- Mobile Menu -->
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>