    <footer>
        <div class="footer-wrapper">
           <!-- Footer Start-->
           <div class="footer-area footer-padding">
               <div class="container ">
                   <div class="row justify-content-between">
                       <div class="col-xl-4 col-lg-3 col-md-8 col-sm-8">
                           <div class="single-footer-caption mb-30">
                               <div class="single-footer-caption mb-30">
                                   <!-- logo -->
                                   <div class="footer-logo mb-35">
                                       <a href="index.php"><img src="huella_xoxoktli.png" class="img-fluid" alt="huella_xoxoktli"></a>
                                   </div>
                                   <div class="footer-tittle">
                                       <div class="footer-pera">
                                           <p>Cambiemos nuestro mundo para salvar el de todos.</p>
                                       </div>
                                   </div>
                                   <!-- social -->
                                   <div class="footer-social">
                                       <a href="#"><i class="fab fa-twitter"></i></a>
                                       <a href="#"><i class="fab fa-facebook-f"></i></a>
                                       <a href="#"><i class="fab fa-pinterest-p"></i></a>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4">
                           <div class="single-footer-caption mb-30">
                               <div class="footer-tittle">
                                   <h4>Xoxoktli</h4>
                                   <ul>
                                       <li><a href="proyecto.php">Proyecto</a></li>
                                       <li><a href="equipo.php">El Equipo</a></li>
                                       <li><a href="wikiverde.php">Wiki Verde</a></li>
                                   </ul>
                               </div>
                           </div>
                       </div>
                       <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4">
                           <div class="single-footer-caption mb-30">
                               <div class="footer-tittle">
                                   <h4>Calculadoras CO2</h4>
                                   <ul>
                                       <li><a href="calculadora.php">Calculadora CO2</a>
                                       <li><a href="casa.php">Casa</a></li>
                                       <li><a href="automivil.php">Automóvil</a></li>
                                       <li><a href="inteligente.php">Inteligente</a></li>
                                   </ul>
                               </div>
                           </div>
                       </div>
                       <div class="col-xl-3 col-lg-4 col-md-6 col-sm-4">
                           <div class="single-footer-caption mb-50">
                               <div class="footer-tittle">
                                   <h4>Hackathon BBVA 2020</h4>
                                    <img class="img-fluid" src="logo-small.svg" alt="Logo BBVA">
                                    <br><br>
                                    <img class="img-fluid" src="text.png" alt="">
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
           <!-- footer-bottom area -->
           <div class="footer-bottom-area">
               <div class="container">
                   <div class="footer-border">
                       <div class="row d-flex align-items-center">
                           <div class="col-xl-12 ">
                               <div class="footer-copy-right text-center">
                                   <p>
                                      Copyright &copy;<script>document.write(new Date().getFullYear());</script> Huella Xoxoktli <i class="fa fa-heart" aria-hidden="true"></i>
                                   </p>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- Footer End-->
          </div>
      </footer>